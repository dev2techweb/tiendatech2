</section>
    <footer class="footer row">
    	<section class="logo__section row b-red full-width">
        <section class="footer__logo col-12 col-lg-6 d-flex j-end a-center">
          <div class="d-flex a-end">
            <figure class="footer__logo__figure e-center" style="height:4rem;">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-white.png" alt="">
            </figure>
          </div>
        </section>
        <section class="footer__info col-12 col-lg-6 d-flex">
          <div class="" style="margin-left:2rem;">
            <h5 class="t-white footer__info__title t-bold">Contáctanos:</h5>
            <ul class="t-white no-list">
              <li><strong class="t-black">Teléfonos: </strong>2506-5888</li>
              <li><strong class="t-black">Correo: </strong>info@tech-hospital.com</li>
              <li><strong class="t-black">Dirección: </strong><a href="https://goo.gl/maps/QN6cSAS9ATT2" class="t-white t-hover-black" target="blank">Diagonal 6 12-42 zona 10 <br>Edificio Design Center, local 216 </a><br>Guatemala, Guatemala, CA</li>
            </ul>
          </div>
        </section>
      </section>
      <section class="credit__section row b-black t-white full-width t-center">
        <p class="full-width t-center credit__text">Este sitio fue creado por <a href="https://techwebgt.com" class="t-white t-hover-red">TechWeb</a> - &copy; 2018</p>
      </section>
    </footer>
  </div>
  <?php wp_footer(); ?>
</body>
</html>
